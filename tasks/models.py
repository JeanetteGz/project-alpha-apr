from django.db import models
from django.contrib.auth.models import User
from projects.models import Project


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, related_name="tasks"
    )
    assignee = models.ForeignKey(
        User, null=True, on_delete=models.CASCADE, related_name="tasks"
    )

    def __str__(self):
        return self.name


class TaskListView(models.Model):
    model = Task
    context_object_name = "tasks"
    template_name = "tasks/my_tasks.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)
