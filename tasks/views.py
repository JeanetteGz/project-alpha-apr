from django.shortcuts import render, redirect
from tasks.forms import TaskForm, Task
from django.contrib.auth.decorators import login_required


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.is_completed = False
            task.save()
            return redirect("projects/list.html")
    else:
        form = TaskForm()
        context = {
            "form": form,
        }
    return render(request, "create_task.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    return render(request, "my_tasks.html", {"tasks": tasks})
